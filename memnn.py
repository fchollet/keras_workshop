from keras.models import Sequential
from keras.layers.embeddings import Embedding
from keras.layers.core import Activation, Dense, Merge, RepeatVector, Permute, Dropout
from keras.layers.recurrent import GRU, LSTM
from keras.datasets.data_utils import get_file
import tarfile
import numpy as np

from workshop_utils import tokenize, parse_stories, get_stories, vectorize_stories


path = get_file('babi-tasks-v1-2.tar.gz', origin='http://www.thespermwhale.com/jaseweston/babi/tasks_1-20_v1-2.tar.gz')
tar = tarfile.open(path)

challenges = {
    # QA1 with 10,000 samples
    "single_supporting_fact_10k": "tasks_1-20_v1-2/en-10k/qa1_single-supporting-fact_{}.txt",
    # QA2 with 10,000 samples
    "two_supporting_facts_10k": "tasks_1-20_v1-2/en-10k/qa2_two-supporting-facts_{}.txt",
}
challenge_type = "single_supporting_fact_10k"
challenge = challenges[challenge_type]

print 'Extracting stories for the challenge:', challenge_type
train_stories = get_stories(tar.extractfile(challenge.format('train')))
test_stories = get_stories(tar.extractfile(challenge.format('test')))

vocab = sorted(reduce(lambda x, y: x | y, (set(story + q + [answer]) for story, q, answer in train_stories + test_stories)))
# Reserve 0 for masking via pad_sequences
vocab_size = len(vocab) + 1
story_maxlen = max(map(len, (x for x, _, _ in train_stories + test_stories)))
query_maxlen = max(map(len, (x for _, x, _ in train_stories + test_stories)))

print '-'
print "Vocab size:", vocab_size, 'unique words'
print "Story max length:", story_maxlen, 'words'
print "Query max length:", query_maxlen, 'words'
print "Number of training stories:", len(train_stories)
print "Number of test stories:", len(test_stories)
print '-'
print "Here's what a 'story' tuple looks like (input, query, answer):"
print train_stories[0]
print '-'
print "let's vectorize the word sequences"
print "(replace words with integer indices and format them into 2D tensors)"

word_idx = dict((c, i + 1) for i, c in enumerate(vocab))
inputs_train, queries_train, answers_train = vectorize_stories(train_stories, word_idx, story_maxlen, query_maxlen)
inputs_test, queries_test, answers_test = vectorize_stories(test_stories, word_idx, story_maxlen, query_maxlen)

print '-'
print 'inputs: integer tensor of shape (samples, max_length)'
print 'inputs_train shape:', inputs_train.shape
print 'inputs_test shape:', inputs_test.shape
print '-'
print 'queries: integer tensor of shape (samples, max_length)'
print 'queries_train shape:', queries_train.shape
print 'queries_test shape:', queries_test.shape
print '-'
print 'answers: binary (1 or 0) tensor of shape (samples, vocab_size)'
print 'answers_train shape:', answers_train.shape
print 'answers_test shape:', answers_test.shape
print '-'
print "Compiling the Keras model..."


# embed the input sequence into a sequence of vectors
input_encoder_m = Sequential()
input_encoder_m.add(Embedding(input_dim=vocab_size,
                              output_dim=64,
                              input_length=story_maxlen))  # output: (samples, story_maxlen, embedding_dim)
print 'Output shape after "input_encoder_m":', input_encoder_m.output_shape

# embed the question into a single vector
question_encoder = Sequential()
question_encoder.add(Embedding(input_dim=vocab_size,
                               output_dim=64,
                               input_length=query_maxlen))  # output: (samples, query_maxlen, embedding_dim)
print 'Output shape after "question_encoder":', question_encoder.output_shape

# compute a "match" between input sequence elements (which are vectors) and the question vector
match = Sequential()
match.add(Merge([input_encoder_m, question_encoder],
                mode='dot',
                dot_axes=[(2,), (2,)]))  # output: (samples, story_maxlen, query_maxlen)
print 'Output shape after "match":', match.output_shape

# embed the input into a single vector with size = story_maxlen:
input_encoder_c = Sequential()
input_encoder_c.add(Embedding(input_dim=vocab_size,
                              output_dim=query_maxlen,
                              input_length=story_maxlen))  # output: (samples, story_maxlen, query_maxlen)
print 'Output shape after "input_encoder_c":', input_encoder_c.output_shape

# sum the match vector with the input vector:
response = Sequential()
response.add(Merge([match, input_encoder_c], mode='sum'))  # output: (samples, story_maxlen, query_maxlen)
print 'Output shape after "Merge" in "response":', response.output_shape
response.add(Permute((2, 1)))  # output: (samples, query_maxlen, story_maxlen)
print 'Output shape after "response":', response.output_shape

# concatenate the match vector with the question vector, and do logistic regression on top
answer = Sequential()
answer.add(Merge([response, question_encoder], mode='concat', concat_axis=-1))
print 'Output shape after "Merge" in "answer":', answer.output_shape
answer.add(LSTM(64))
answer.add(Dropout(0.25))
print 'Output shape after RNN in "answer":', answer.output_shape
answer.add(Dense(vocab_size))
answer.add(Activation('softmax'))  # we output a probability distribution over the vocabulary

answer.compile(optimizer='rmsprop', loss='categorical_crossentropy')

print 'Training - reporting test accuracy after every iterations over the training data...'
# Note: you could use a Graph model to avoid repeat the input twice
answer.fit([inputs_train, queries_train, inputs_train], answers_train,
           batch_size=32,
           nb_epoch=70,
           show_accuracy=True,
           validation_data=([inputs_test, queries_test, inputs_test], answers_test))
