"""Implementation of a LSTM-based encoder setup applied to the bAbI dataset

Author: Stephen Merity
Some adaptations by Francois Chollet

Task Number                  | FB LSTM Baseline | Keras LSTM
---                          | ---              | ---
QA1 - Single Supporting Fact | 50               | 52.1  *
QA2 - Two Supporting Facts   | 20               | 37.0  *
QA3 - Three Supporting Facts | 20               | 20.5  *
QA4 - Two Arg. Relations     | 61               | 62.9  *
QA5 - Three Arg. Relations   | 70  *            | 61.9
QA6 - Yes/No Questions       | 48               | 50.7  *
QA7 - Counting               | 49               | 78.9  *
QA8 - Lists/Sets             | 45               | 77.2  *
QA9 - Simple Negation        | 64               | 64.0
QA10 - Indefinite Knowledge  | 44               | 47.7  *
QA11 - Basic Coreference     | 72               | 74.9  *
QA12 - Conjunction           | 74               | 76.4  *
QA13 - Compound Coreference  | 94               | 94.4  *
QA14 - Time Reasoning        | 27               | 34.8  *
QA15 - Basic Deduction       | 21               | 32.4  *
QA16 - Basic Induction       | 23               | 50.6  *
QA17 - Positional Reasoning  | 51  *            | 49.1
QA18 - Size Reasoning        | 52               | 90.8  *
QA19 - Path Finding          | 8                | 9.0   *
QA20 - Agent's Motivations   | 91  *            | 90.7

For the resources related to the bAbI project, refer to:
https://research.facebook.com/researchers/1543934539189348
"""

from keras.models import Sequential
from keras.layers.embeddings import Embedding
from keras.layers.core import Activation, Dense, Merge
from keras.layers.recurrent import GRU, LSTM
from keras.datasets.data_utils import get_file
import tarfile
import numpy as np

from workshop_utils import tokenize, parse_stories, get_stories, vectorize_stories

path = get_file('babi-tasks-v1-2.tar.gz', origin='http://www.thespermwhale.com/jaseweston/babi/tasks_1-20_v1-2.tar.gz')
tar = tarfile.open(path)

challenges = {
    # QA1 with 10,000 samples
    "single_supporting_fact_10k": "tasks_1-20_v1-2/en-10k/qa1_single-supporting-fact_{}.txt",
    # QA2 with 10,000 samples
    "two_supporting_facts_10k": "tasks_1-20_v1-2/en-10k/qa2_two-supporting-facts_{}.txt",
}
challenge_type = "single_supporting_fact_10k"
challenge = challenges[challenge_type]

print 'Extracting stories for the challenge:', challenge_type
train_stories = get_stories(tar.extractfile(challenge.format('train')))
test_stories = get_stories(tar.extractfile(challenge.format('test')))

vocab = sorted(reduce(lambda x, y: x | y, (set(story + q + [answer]) for story, q, answer in train_stories + test_stories)))
# Reserve 0 for masking via pad_sequences
vocab_size = len(vocab) + 1
story_maxlen = max(map(len, (x for x, _, _ in train_stories + test_stories)))
query_maxlen = max(map(len, (x for _, x, _ in train_stories + test_stories)))

print '-'
print "Vocab size:", vocab_size, 'unique words'
print "Story max length:", story_maxlen, 'words'
print "Query max length:", query_maxlen, 'words'
print "Number of training stories:", len(train_stories)
print "Number of test stories:", len(test_stories)
print '-'
print "Here's what a 'story' tuple looks like (input, query, answer):"
print train_stories[0]
print '-'
print "let's vectorize the word sequences"
print "(replace words with integer indices and format them into 2D tensors)"

word_idx = dict((c, i + 1) for i, c in enumerate(vocab))
inputs_train, queries_train, answers_train = vectorize_stories(train_stories, word_idx, story_maxlen, query_maxlen)
inputs_test, queries_test, answers_test = vectorize_stories(test_stories, word_idx, story_maxlen, query_maxlen)

print '-'
print 'inputs: integer tensor of shape (samples, max_length)'
print 'inputs_train shape:', inputs_train.shape
print 'inputs_test shape:', inputs_test.shape
print '-'
print 'queries: integer tensor of shape (samples, max_length)'
print 'queries_train shape:', queries_train.shape
print 'queries_test shape:', queries_test.shape
print '-'
print 'answers: binary (1 or 0) tensor of shape (samples, vocab_size)'
print 'answers_train shape:', answers_train.shape
print 'answers_test shape:', answers_test.shape
print '-'
print "Compiling the Keras model..."

input_encoder = Sequential()
input_encoder.add(Embedding(input_dim=vocab_size, output_dim=64))
input_encoder.add(LSTM(64, return_sequences=False))  # output shape: (samples, 64)

question_encoder = Sequential()
question_encoder.add(Embedding(input_dim=vocab_size, output_dim=64))
question_encoder.add(LSTM(64, return_sequences=False))  # output shape: (samples, 64)

model = Sequential()
model.add(Merge([input_encoder, question_encoder],
                mode='concat',
                concat_axis=-1))  # output shape: (samples, 64*2)
model.add(Dense(vocab_size))
model.add(Activation('softmax'))  # output a probability distribution over all words

model.compile(optimizer='adam', loss='categorical_crossentropy')

print 'Training - reporting test accuracy after every iterations over the training data...'
model.fit([inputs_train, queries_train], answers_train,
          batch_size=32,
          nb_epoch=50,
          show_accuracy=True,
          validation_data=([inputs_test, queries_test], answers_test))
